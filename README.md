# Challenge 
Cápsula Data Handling

## Model
![chat_gpt](src/main/resources/doc/model.png?raw=true "chat_gpt")

## Steps
### Create schema in MySql
```bash
  CREATE SCHEMA IF NOT EXISTS `chatgpt_db` DEFAULT CHARACTER SET utf8 ;
```
### Settings
#### application.properties 
  - set user and password
```bash
    spring.datasource.username= {user}
    spring.datasource.password= {passowrd}
```
### Run
```bash
    mvn test -Dtest=Queries
``` 