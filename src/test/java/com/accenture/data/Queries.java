package com.accenture.data;

import com.accenture.data.constants.Query;
import com.accenture.data.dto.UserWithoutBalanceDTO;
import com.accenture.data.model.Record;
import com.accenture.data.model.*;
import com.accenture.data.repository.*;
import org.junit.jupiter.api.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDateTime;
import java.util.List;

@SpringBootTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class Queries {
    Logger logger = LoggerFactory.getLogger(Queries.class);
    @Autowired
    UserRepository userRepository;
    @Autowired
    RoleRepository roleRepository;
    @Autowired
    UserRoleRepository userRoleRepository;
    @Autowired
    QuestionRepository questionRepository;
    @Autowired
    RecordRepository recordRepository;
    @Autowired
    BalanceRepository balanceRepository;

    @Test
    @Order(1)
    @DisplayName(Query.ListUserWithRoleAdmin.Requirement)
    public void list_of_user_by_role_admin(){
        System.out.println(Query.ListUserWithRoleAdmin.Requirement);
        Role role =  roleRepository.findByName(Query.ROLE_ADMIN);
        List<UserRole> usersByRole = userRoleRepository.findById_RoleId(role.getId());
        System.out.printf(Query.ListUserWithRoleAdmin.FORMAT, "USERNAME", "NAME", "ID");
        usersByRole
                .stream()
                .forEach(userRole -> {
                    System.out.printf(Query.ListUserWithRoleAdmin.FORMAT, userRole.getUser().getUsername() ,
                            userRole.getUser().getName(), userRole.getUser().getId());
                });
        Assertions.assertEquals(3,usersByRole.size());
        System.out.println("\n");
    }

    @Test
    @Order(2)
    @DisplayName(Query.ListUserWithRoleUser.Requirement)
    public void list_of_user_by_role_user(){
        System.out.println(Query.ListUserWithRoleUser.Requirement);

        Role role =  roleRepository.findByName(Query.ROLE_USER);
        List<UserRole> usersByRole = userRoleRepository.findById_RoleId(role.getId());

        System.out.printf(Query.ListUserWithRoleUser.FORMAT, "USERNAME", "NAME", "ID");
        usersByRole
                .stream()
                .forEach(userRole -> {
                    System.out.printf(Query.ListUserWithRoleUser.FORMAT, userRole.getUser().getUsername() ,
                            userRole.getUser().getName(), userRole.getUser().getId());
                });
        Assertions.assertEquals(4,usersByRole.size());
        System.out.println("\n");
    }

    @Test
    @Order(3)
    @DisplayName(Query.NumberOfTimeAQuestionWasAsked.Requirement)
    public void total_times_questions_was_asked(){
        System.out.println(Query.NumberOfTimeAQuestionWasAsked.Requirement);
        Question question = questionRepository.findAll().get(5);
        Long total = recordRepository.countByQuestion(question);

        System.out.printf(Query.NumberOfTimeAQuestionWasAsked.FORMAT, "ID", "QUESTION", "# OF TIMES");
        System.out.printf(Query.NumberOfTimeAQuestionWasAsked.FORMAT, question.getId(), question.getDetails(), total);

        Assertions.assertEquals(4, total);
        System.out.println("\n");
    }

    @Test
    @Order(4)
    @DisplayName(Query.ListOfUserAskedQuestionWithoutBalance.Requirement)
    public void list_of_users_asked_question_without_balance(){
        System.out.println(Query.ListOfUserAskedQuestionWithoutBalance.Requirement );
        System.out.printf(Query.ListOfUserAskedQuestionWithoutBalance.FORMAT, "USERNAME", "NAME", "COUNTRY", "ROLE");

        List<UserWithoutBalanceDTO> users = userRepository.findUserWithoutBalance();
        users.forEach
                (user -> System.out.printf(Query.ListOfUserAskedQuestionWithoutBalance.FORMAT, user.getUserName(),user.getName(),
                        user.getCountry(),user.getRoleName()));

        System.out.println("Hasta 2 preguntas será gratuito.");
        System.out.println("\n");
    }


    @Test
    @Order(5)
    @DisplayName(Query.ListOfAdminUserToAddQuestionsAndAskedSomeOfThem.Requirement)
    public void test(){
        System.out.println(Query.ListOfAdminUserToAddQuestionsAndAskedSomeOfThem.Requirement);

        List<Record> list = recordRepository.findUserThatAskTheSameQuestionCreatedBySelf();

        System.out.printf(Query.ListOfAdminUserToAddQuestionsAndAskedSomeOfThem.FORMAT, "NAME", "SURNAME", "PHONE NUMBER", "QUESTION");
        list.forEach(record -> System.out.printf(Query.ListOfAdminUserToAddQuestionsAndAskedSomeOfThem.FORMAT ,
                record.getUser().getName(), record.getUser().getSurname(),record.getUser().getPhoneNumber(),
                record.getQuestion().getDetails()));
        System.out.println("\n");
    }
    @Test
    @Order(6)
    @DisplayName(Query.BalanceByUser.Requirement)
    public void balance_by_users(){
        System.out.println(Query.BalanceByUser.Requirement);
        User user = userRepository.findById(3L).get();
        List<Balance> list = balanceRepository.findByUser(user);
        System.out.printf(Query.BalanceByUser.FORMAT, "NAME", "SURNAME", "VALUE", "CURRENCY");
        list.forEach(
                balance -> System.out.printf(Query.BalanceByUser.FORMAT, balance.getUser().getName(),
                        balance.getUser().getSurname(), balance.getValue(), balance.getCurrency().getName()));
        System.out.println("\n");
    }

    @Test
    @Order(7)
    @DisplayName(Query.ListOfUserAskedQuestionBetweenDates.Requirement)
    public void records_by_user_in_date_range(){
         System.out.println(Query.ListOfUserAskedQuestionBetweenDates.Requirement);
         LocalDateTime start = LocalDateTime.of(2023,07,18,10,25,54);
         LocalDateTime end = LocalDateTime.of(2023,07,25,00,00,00);

         System.out.printf(Query.ListOfUserAskedQuestionBetweenDates.FORMAT, "USERNAME", "QUESTION", "CREATED");
         List<Record> records = recordRepository.findAllByCreatedBetween(start,end);
         records.forEach
                 (record -> System.out.printf(Query.ListOfUserAskedQuestionBetweenDates.FORMAT, record.getUser().getUsername(),
                                 record.getQuestion().getDetails(), record.getCreated()));
        System.out.println("\n");
    }

    @Test
    @Order(8)
    @DisplayName(Query.ListOfQuestionRegisteredNeverAsked.Requirement)
    public void list_of_question_created_never_asked(){
        System.out.println(Query.ListOfQuestionRegisteredNeverAsked.Requirement);
        List<Question> questionsNeverAsked = questionRepository.findQuestionNeverAsked();
        System.out.printf(Query.ListOfQuestionRegisteredNeverAsked.FORMAT, "ID", "QUESTION");
        questionsNeverAsked.forEach(
                question -> System.out.printf(Query.ListOfQuestionRegisteredNeverAsked.FORMAT,  question.getId() , question.getDetails()));
        System.out.println("\n");
    }

}
