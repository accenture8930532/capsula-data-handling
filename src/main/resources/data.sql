start transaction;
use chatgpt_db;

delete  from chatgpt_db.role;
insert into chatgpt_db.role (id, name) values (1,"SCOPE_ROLE_ADMIN");
insert into chatgpt_db.role (id, name) values (2,"SCOPE_ROLE_USER");


delete from  chatgpt_db.user;
insert into chatgpt_db.user (id,username,password,created_date,name,surname,phone_number,gender,country)
values (1,"userA@aaaa.com","888888",CURRENT_TIMESTAMP,"aaa","aaaaaa","11111111", "M","Argentina");

insert into chatgpt_db.user (id,username,password,created_date,name,surname,phone_number,gender,country)
values (2,"userB@bbbb.com","123456",CURRENT_TIMESTAMP,"bbb","bbbbbb","2222222", "F","Brasil");

insert into chatgpt_db.user (id, username,password,created_date,name,surname,phone_number,gender,country)
values (3,"userC@cccc.com","123456",CURRENT_TIMESTAMP,"ccc","ccccccc","3333333", "F","Canadá");

insert into chatgpt_db.user (id,username,password,created_date,name,surname,phone_number,gender,country)
values (4,"userD@dddd.com","123456",CURRENT_TIMESTAMP,"ddd","dddddd","444444", "F","USA");

insert into chatgpt_db.user (id,username,password,created_date,name,surname,phone_number,gender,country)
values (5,"userE@eeee.com","232322",CURRENT_TIMESTAMP,"eee","eeeeee",555555, "M","Mexico");


delete from chatgpt_db.user_role;
insert into chatgpt_db.user_role (user_id, role_id)
values (1,1),(1,2),(2,2),(3,1),(3,2),(4,2),(5,1);
 
delete from chatgpt_db.answer;
insert into chatgpt_db.answer (id, details) values (1, "no");
insert into chatgpt_db.answer (id, details) values (2, "si");
insert into chatgpt_db.answer (id, details) values (3, "no lo sé");
 
 
delete from chatgpt_db.question;
insert into chatgpt_db.question (id,details, created,created_by,answer_id)
values(1, "Pregunta A",CURRENT_TIMESTAMP,1,2);

insert into chatgpt_db.question (id,details, created,created_by,answer_id)
values(2, "Pregunta B",CURRENT_TIMESTAMP,1,2);  

insert into chatgpt_db.question (id,details, created,created_by,answer_id)
values(3, "Pregunta C",CURRENT_TIMESTAMP,3,1);

insert into chatgpt_db.question (id,details, created,created_by)
values(4, "Pregunta D",CURRENT_TIMESTAMP,2);

insert into chatgpt_db.question (id,details, created,created_by)
values(5, "Pregunta E",CURRENT_TIMESTAMP,4);  

insert into chatgpt_db.question (id,details, created,created_by)
values(6, "Pregunta F",CURRENT_TIMESTAMP,3); 

insert into chatgpt_db.question (id,details, created,created_by)
values(7, "Pregunta G",CURRENT_TIMESTAMP,5); 

delete from chatgpt_db.record;
insert into chatgpt_db.record (user_id, question_id,created)
values(1,1,'2023-07-18 10:25:54');
insert into chatgpt_db.record (user_id, question_id,created)
values(1,6,'2023-07-14 11:22:23');
insert into chatgpt_db.record (user_id, question_id,created)
values(2,6,current_timestamp);
insert into chatgpt_db.record (user_id, question_id,created)
values(2,6,'2023-07-20 18:14:29');
insert into chatgpt_db.record (user_id, question_id,created)
values(2,2,'2023-07-20 18:14:29');
insert into chatgpt_db.record (user_id, question_id,created)
values(2,1,'2023-03-20 12:10:43');
insert into chatgpt_db.record (user_id, question_id,created)
values(3,4,'2023-07-24 12:16:23');
insert into chatgpt_db.record (user_id, question_id,created)
values(3,6,'2023-07-27 02:30:59');
insert into chatgpt_db.record (user_id, question_id,created)
values(3,2,'2023-07-29 04:31:24');
insert into chatgpt_db.record (user_id, question_id,created)
values(3,1,'2023-07-13 09:23:37');
insert into chatgpt_db.record (user_id, question_id,created)
values(4,4,current_timestamp);
insert into chatgpt_db.record (user_id, question_id,created)
values(5,4,current_timestamp);


delete  from chatgpt_db.currency;
insert into chatgpt_db.currency (id, name) values (1,"Peso");
insert into chatgpt_db.currency (id, name) values (2,"USD");
insert into chatgpt_db.currency (id, name) values (3,"EUR");

delete from chatgpt_db.balance;
insert into chatgpt_db.balance (user_id,currency_id,value)
values(2,1,200);
insert into chatgpt_db.balance (user_id,currency_id,value)
values(2,3,5);
insert into chatgpt_db.balance (user_id, currency_id,value)
values(3,2,6);

commit;

