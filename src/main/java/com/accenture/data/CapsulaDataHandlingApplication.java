package com.accenture.data;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CapsulaDataHandlingApplication {

	public static void main(String[] args) {
		SpringApplication.run(CapsulaDataHandlingApplication.class, args);
	}

}
