package com.accenture.data.repository;

import com.accenture.data.model.Balance;
import com.accenture.data.model.User;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BalanceRepository extends JpaRepository<Balance,Long> {
    List<Balance> findByUser(User user);
}
