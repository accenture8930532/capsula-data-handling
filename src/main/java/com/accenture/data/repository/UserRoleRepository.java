package com.accenture.data.repository;

import com.accenture.data.model.UserRole;
import com.accenture.data.model.UserRoleKey;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRoleRepository extends JpaRepository<UserRole, UserRoleKey> {
    List<UserRole> findById_RoleId(Long roleId);
}
