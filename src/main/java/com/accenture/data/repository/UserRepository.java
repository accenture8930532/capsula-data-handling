package com.accenture.data.repository;

import com.accenture.data.dto.UserWithoutBalanceDTO;
import com.accenture.data.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import java.util.List;
@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    @Query("SELECT " +
            "new com.accenture.data.dto.UserWithoutBalanceDTO(user.username, user.name,user.country, userRole.role.name) " +
            "FROM User user " +
            "INNER JOIN Record record  ON record.user = user " +
            "LEFT JOIN Balance balance ON balance.user = user " +
            "JOIN UserRole userRole ON userRole.user = user " +
            "WHERE balance.value IS NULL " +
            "GROUP BY user.id, userRole.role")
    List<UserWithoutBalanceDTO> findUserWithoutBalance();
}
