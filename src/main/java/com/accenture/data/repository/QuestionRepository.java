package com.accenture.data.repository;

import com.accenture.data.model.Question;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface QuestionRepository extends JpaRepository<Question,Long> {

    @Query("SELECT question FROM Record record " +
            "RIGHT JOIN Question question ON question = record.question WHERE record.question IS NULL")
    public List<Question> findQuestionNeverAsked();
}
