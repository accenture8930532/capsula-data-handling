package com.accenture.data.repository;

import com.accenture.data.model.Question;
import com.accenture.data.model.Record;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface RecordRepository extends JpaRepository<Record,Long> {

    Long countByQuestion(Question question);

    List<Record> findAllByCreatedBetween(LocalDateTime start, LocalDateTime end);
    @Query("SELECT record  " +
            "FROM Record record " +
            "INNER JOIN Question question ON question = record.question AND record.user = question.createdBy " +
            "INNER JOIN UserRole userRole ON question.createdBy = userRole.user  AND userRole.role.id = 1")
    List<Record> findUserThatAskTheSameQuestionCreatedBySelf();

}
