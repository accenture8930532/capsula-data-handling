package com.accenture.data.constants;

public class Query {
    public static final String ROLE_ADMIN = "SCOPE_ROLE_ADMIN";
    public static final String ROLE_USER = "SCOPE_ROLE_USER";
    public static class ListUserWithRoleAdmin{
        public static final String Requirement = "1- Listado de Usuarios con Role ADMIN";
        public static final  String FORMAT = "| %-14s | %-8s | %4s |%n";
    }

    public static class ListUserWithRoleUser{
        public static final String Requirement = "1- Listado de Usuarios con Role USER";
        public static final  String FORMAT = "| %-14s | %-8s | %4s |%n";
    }

    public static class NumberOfTimeAQuestionWasAsked{
        public static final String Requirement ="2- Cantidad de veces que fue preguntada una pregunta";
        public static final  String FORMAT = "| %-4s | %-10s | %-10s |%n";
    }

    public static class ListOfUserAskedQuestionWithoutBalance{
        public static final String Requirement ="3- Lista de usuarios (userName, nombre, país y rol/roles) que " +
                "realizaron preguntas y NO TIENEN saldo deudor";
        public static final  String FORMAT = "| %-14s | %-8s | %-10s | %-16s |%n";
    }

    public static class ListOfAdminUserToAddQuestionsAndAskedSomeOfThem{
        public static final String Requirement ="4- Lista de usuarios ADMIN"+
                " que dieron de alta preguntas y usaron el sistema para realizar al menos alguna de las mismas pregunta " +
                "que crearon (nombre, apellido, telefono y pregunta)";
        public static final  String FORMAT = "| %-10s | %-8s | %-12s | %-10s |%n";
    }

    public static class BalanceByUser{
        public static final String Requirement ="5- Saldo deudor de un determinado usuario (nombre, apellido, " +
                "importe y moneda)";
        public static final  String FORMAT = "| %-10s | %-8s | %-5s | %-8s |%n";
    }

    public static class ListOfUserAskedQuestionBetweenDates{
        public static final String Requirement ="6- Lista de usuarios que hicieron " +
                "preguntas en un rango de fechas";
        public static final  String FORMAT = "| %-14s | %-10s | %-19s |%n";
    }

    public static class ListOfQuestionRegisteredNeverAsked{
        public static final String Requirement ="7- Lista de preguntas registradas que" +
                " nunca fueron preguntadas";
        public static final  String FORMAT = "| %-4s | %-10s |%n";
    }

}
