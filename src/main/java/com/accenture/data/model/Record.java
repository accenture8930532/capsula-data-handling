package com.accenture.data.model;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
public class Record {
    @Id
    private Long id;
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne
    @JoinColumn(name = "question_id")
    private Question question;

    private LocalDateTime created;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    public LocalDateTime getCreated() {return created;}

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    @Override
    public String
    toString() {
        return "Record{" +
                "id=" + id +
                ", user=" + user +
                ", question=" + question +
                ", created=" + created +
                '}';
    }
}
