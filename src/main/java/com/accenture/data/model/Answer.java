package com.accenture.data.model;

import javax.persistence.Entity;
import javax.persistence.Id;
@Entity
public class Answer {
    @Id
    private Long id;
    private String details;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }
}
