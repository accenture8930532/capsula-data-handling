package com.accenture.data.model;

import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class UserRoleKey implements Serializable {
    private static final long serialVersionUID = 4116730124993107884L;

    @JoinColumn(name="user_id")
    private Long userId;

    @JoinColumn(name="role_id")
    private Long roleId;

    public UserRoleKey() {
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UserRoleKey)) return false;
        UserRoleKey that = (UserRoleKey) o;
        return userId.equals(that.userId) && roleId.equals(that.roleId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, roleId);
    }
}
