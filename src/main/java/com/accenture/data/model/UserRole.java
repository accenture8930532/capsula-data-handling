package com.accenture.data.model;

import javax.persistence.*;

@Entity
public class UserRole {
    @EmbeddedId
    private UserRoleKey id;

    @ManyToOne
    @MapsId("userId")
    @JoinColumn(name="user_id")
    private User user;
    @ManyToOne
    @MapsId("roleId")
    @JoinColumn(name="role_id")
    private Role role;


    public UserRole(){}
    public UserRole(UserRoleKey id) {
        this.id = id;
    }
    public UserRoleKey getId() {
        return id;
    }
    public void setId(UserRoleKey id) {
        this.id = id;
    }
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }
}
