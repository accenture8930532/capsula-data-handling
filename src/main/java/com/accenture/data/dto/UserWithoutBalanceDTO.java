package com.accenture.data.dto;

public class UserWithoutBalanceDTO {
    private String userName;
    private String name;
    private String country;
    private String roleName;

    public UserWithoutBalanceDTO(String userName, String name, String country, String roleName) {
        this.userName = userName;
        this.name = name;
        this.country = country;
        this.roleName = roleName;
    }

    public UserWithoutBalanceDTO() {
    }

    public String getUserName() {
        return userName;
    }

    public String getName() {
        return name;
    }

    public String getCountry() {
        return country;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }
}
